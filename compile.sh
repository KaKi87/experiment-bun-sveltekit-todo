#!/usr/bin/env bash

if [[ -z "$VERSION" ]]; then
  head=$(cat .git/HEAD)
  head=${head:5}
  hash=$(cat .git/$head)
  VERSION=${hash:0:7}
fi

bun build ./build/index.js --compile --outfile ./data/todo-"$VERSION"