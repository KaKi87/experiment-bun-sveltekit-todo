import {
    error,
    json
} from '@sveltejs/kit';

import Joi from 'joi';

import {
    getTasks
} from '$lib/database.js';

export const GET = async event => {
    const {
        value: {
            limit
        } = {},
        error: queryError
    } = Joi.object({
        limit: Joi.number().integer().positive()
    }).validate(Object.fromEntries(event.url.searchParams));
    if(queryError)
        return error(400, queryError);
    return json(
        await getTasks({
            limit
        })
    );
};