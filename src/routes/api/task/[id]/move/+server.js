import {
    error,
    json
} from '@sveltejs/kit';

import Joi from 'joi';
import { LexoRank } from 'lexorank';

import {
    updateTask
} from '$lib/database.js';

export const
    PATCH = async event => {
        const {
            value: pathParams,
            error: pathParamsError
        } = Joi.object({
            id: Joi.string().required()
        }).validate(event.params);
        if(pathParamsError)
            return error(400, pathParamsError);
        let {
            value: {
                direction,
                targetRank1,
                targetRank2
            } = {},
            error: queryError
        } = Joi.object({
            direction: Joi.valid('up', 'down'),
            targetRank1: Joi.string().required(),
            targetRank2: Joi.string()
        }).validate(Object.fromEntries(event.url.searchParams));
        targetRank1 = LexoRank.parse(targetRank1);
        const rank = (
            targetRank2
                ? LexoRank.parse(targetRank2).between(targetRank1)
                : {
                    'up': () => targetRank1.genPrev(),
                    'down': () => targetRank1.genNext()
                }[direction]()
        ).toString();
        if(queryError)
            return error(400, queryError);
        await updateTask({
            ...pathParams,
            rank
        });
        return json({
            rank
        });
    };