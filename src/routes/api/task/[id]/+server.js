import {
    error,
    json
} from '@sveltejs/kit';

import Joi from 'joi';

import {
    getTask,
    updateTask,
    removeTask
} from '$lib/database.js';

export const
    GET = async event => {
        const {
            value: pathParams,
            error: pathParamsError
        } = Joi.object({
            id: Joi.string().required()
        }).validate(event.params);
        if(pathParamsError)
            return error(400, pathParamsError);
        return json(await getTask(pathParams));
    },
    PATCH = async event => {
        const {
            value: pathParams,
            error: pathParamsError
        } = Joi.object({
            id: Joi.string().required()
        }).validate(event.params);
        if(pathParamsError)
            return error(400, pathParamsError);
        const {
            value: body,
            error: bodyError
        } = Joi.object({
            rank: Joi.string(),
            isDone: Joi.boolean(),
            name: Joi.string()
        }).validate(await event.request.json());
        if(bodyError)
            return error(400, bodyError);
        await updateTask({
            ...pathParams,
            ...body
        });
        return new Response(null, { status: 204 });
    },
    DELETE = async event => {
        const {
            value: pathParams,
            error: pathParamsError
        } = Joi.object({
            id: Joi.string().required()
        }).validate(event.params);
        if(pathParamsError)
            return error(400, pathParamsError);
        await removeTask(pathParams);
        return new Response(null, { status: 204 });
    };