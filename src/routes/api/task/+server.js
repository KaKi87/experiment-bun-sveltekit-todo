import {
    error,
    json
} from '@sveltejs/kit';

import Joi from 'joi';
import { nanoid } from 'nanoid';
import { LexoRank } from 'lexorank';

import {
    addTask
} from '$lib/database.js';

export const
    POST = async event => {
        const {
            value: task,
            error: bodyError
        } = Joi.object({
            isDone: Joi.boolean().required(),
            name: Joi.string().required()
        }).validate(await event.request.json());
        if(bodyError)
            return error(400, bodyError);
        const {
            value: {
                nextRank
            } = {},
            error: queryError
        } = Joi.object({
            nextRank: Joi.string()
        }).validate(Object.fromEntries(event.url.searchParams));
        if(queryError)
            return error(400, queryError);
        task.id = nanoid();
        task.rank = (
            nextRank
                ? LexoRank.parse(nextRank).genPrev()
                : LexoRank.middle()
        ).toString();
        await addTask(task);
        return json({
            id: task.id,
            rank: task.rank
        });
    };