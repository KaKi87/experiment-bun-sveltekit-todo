import { browser } from '$app/environment';
import { hydrated } from '$lib/hydrated';

export const load = async event => {
    const tasks = (async () => await (await event.fetch('/api/tasks')).json())();
    return {
        tasks: browser && hydrated ? tasks : await tasks
    };
};