import {
    error
} from '@sveltejs/kit';

import Joi from 'joi';

import { getTaskMoveParams } from '$lib/taskHelpers.js';

export const actions = {
    addTask: async event => {
        let {
            value: {
                isDone,
                name,
                nextRank
            },
            error: formError
        } = Joi.object({
            isDone: Joi.valid('on'),
            name: Joi.string().required(),
            nextRank: Joi.string()
        }).validate(Object.fromEntries(await event.request.formData()));
        if(formError)
            return error(400, formError.message);
        isDone = !!isDone;
        await event.fetch(
            `/api/task${nextRank ? `?${new URLSearchParams({ nextRank })}` : ''}`,
            {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    isDone,
                    name
                })
            }
        );
    },
    updateTask: async event => {
        let {
            value: {
                action,
                isDone,
                name,
                id
            },
            error: formError
        } = Joi.object({
            action: Joi.valid(
                'rename',
                'toggleDone',
                'moveUp',
                'moveDown',
                'remove'
            ),
            isDone: Joi.valid('on'),
            name: Joi.string().required(),
            id: Joi.string().required()
        }).validate(Object.fromEntries(await event.request.formData()));
        if(formError)
            return error(400, formError);
        const baseUrl = `/api/task/${id}`;
        switch(action){
            case 'rename': {
                await event.fetch(baseUrl, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name
                    })
                });
                break;
            }
            case 'toggleDone': {
                await event.fetch(baseUrl, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        isDone: !isDone
                    })
                });
                break;
            }
            case 'moveUp':
            case 'moveDown': {
                const
                    direction = {
                        'moveUp': 'up',
                        'moveDown': 'down'
                    }[action],
                    {
                        targetRank1,
                        targetRank2
                    } = getTaskMoveParams({
                        tasks: await (await event.fetch('/api/tasks')).json(),
                        taskId: id,
                        direction
                    });
                if(!targetRank1) return error(400);
                const queryParams = new URLSearchParams({
                    direction,
                    targetRank1,
                    ...targetRank2 ? { targetRank2 } : {}
                }).toString();
                await event.fetch(`${baseUrl}/move?${queryParams}`, { method: 'PATCH' });
                break;
            }
            case 'remove': {
                await event.fetch(baseUrl, { method: 'DELETE' });
                break;
            }
        }
    }
};