import {
    init as initDatabase
} from '$lib/database.js';

await initDatabase();