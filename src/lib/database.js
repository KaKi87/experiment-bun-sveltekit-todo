import { Database } from 'bun:sqlite';

const database = new Database('data/todo.sqlite');

export const
    init = async () => {
        await database.query(`
            CREATE TABLE IF NOT EXISTS task (
                id TEXT NOT NULL PRIMARY KEY,
                rank TEXT NOT NULL UNIQUE,
                is_done INTEGER NOT NULL,
                name TEXT NOT NULL
            );
        `).run();
    },
    getTasks = ({
        limit: $limit
    }) => database.query(`
        SELECT id, rank, is_done AS isDone, name
        FROM task
        ORDER BY rank
        ${$limit ? `LIMIT $limit` : ''}
    `).all({
        $limit
    }),
    getTask = ({ id: $id }) => database.query('SELECT rank, is_done as isDone, name FROM task WHERE id = $id').get({ $id }),
    addTask = async ({
        id: $id,
        rank: $rank,
        isDone: $isDone,
        name: $name
    }) => await database.query(`
        INSERT INTO task VALUES (
            $id,
            $rank,
            $isDone,
            $name
        );
    `).run({
        $id,
        $rank,
        $isDone,
        $name
    }),
    updateTask = async ({
        id: $id,
        rank: $rank,
        isDone: $isDone,
        name: $name
    }) => database.query(`
        UPDATE task
        SET
            ${[
                $rank === undefined ? '' : 'rank = $rank',
                $isDone === undefined ? '' : 'is_done = $isDone',
                $name === undefined ? '' : 'name = $name'
            ].filter(Boolean).join(',')}
        WHERE id = $id;
    `).run({
        $id,
        ...$rank === undefined ? {} : { $rank },
        ...$isDone === undefined ? {} : { $isDone },
        ...$name === undefined ? {} : { $name },
    }),
    removeTask = ({ id: $id }) => database.query('DELETE FROM task WHERE id = $id').run({ $id });