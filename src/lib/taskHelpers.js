export const
    getTaskMoveParams = ({
        tasks,
        taskId,
        direction
    }) => {
        const index = tasks.findIndex(task => task.id === taskId);
        return {
            targetRank1: tasks[index + { 'up': -1, 'down': 1 }[direction]]?.rank,
            targetRank2: tasks[index + { 'up': -2, 'down': 2 }[direction]]?.rank
        };
    };